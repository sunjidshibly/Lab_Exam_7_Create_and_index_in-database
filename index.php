<html>
    <head>
        <meta charset="UTF-8">
        <title>Database connection</title>
        <style>
           *{margin: 0px auto;
           padding-top: 10px;
           }  
           td{text-align:center;
           }
          h1,h4{text-align:center;
           }
        </style>
    </head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <body>
        <h1>Index</h1>
        
          <?php
        include_once 'profession.php';
        $list = new Profession();
        $profession_array = $list->index();
        ?>
        <h4><a href="create.php">Create New</a></h4>
        <table style="width: 60%" border="1" >
            
            <th>ID</th>
            <th>Name</th>
            <th>Profession</th>
             <th>Action</th>
        
        <tr>
            <?php foreach ($profession_array as $Profession): ?>
            <td> <?php echo $Profession['id']; ?></td>
            <td> <?php echo $Profession['name']; ?></td>
            <td> <?php echo $Profession['profession']; ?></td>
            <td> <button type="button" class="btn btn-info">View</button> <button type="button" class="btn btn-warning">Update</button> <button type="button" class="btn btn-danger">Delete</button></td>
        </tr>  

        <?php    endforeach; ?>
    </table>   
        
        
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    </body>
</html>