-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2016 at 06:06 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `examdb13`
--

-- --------------------------------------------------------

--
-- Table structure for table `examtbl`
--

CREATE TABLE IF NOT EXISTS `examtbl` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `profession` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examtbl`
--

INSERT INTO `examtbl` (`id`, `name`, `profession`) VALUES
(1, 'sunjid', 'Developer'),
(2, 'abcd', 'abcd'),
(3, 'Obaidul', 'Php developer'),
(4, 'Obaidul', 'Php developer'),
(5, 'Obaidul', 'Php developer'),
(6, 'acbn', 'bbvb'),
(7, 'Masud', 'Database Designer'),
(8, 'Shibly Mohammad', 'PHP Developer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `examtbl`
--
ALTER TABLE `examtbl`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `examtbl`
--
ALTER TABLE `examtbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
